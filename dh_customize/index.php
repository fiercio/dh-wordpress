<?php
/** Pobranie argumentow **/
$input = json_decode( file_get_contents('php://input') );
require 'class-phpass.php';

/** Polaczenie z baza danych **/
$mysql_host = $input->env->BazaMysql->host;
$mysql_username = $input->env->BazaMysql->uzytkownik;
$mysql_password = $input->env->BazaMysql->haslo;
$mysql_database = $input->env->BazaMysql->nazwa;

mysql_connect($mysql_host, $mysql_username, $mysql_password) or die('Error connecting to MySQL server: ' . mysql_error());
mysql_select_db($mysql_database) or die('Error selecting MySQL database: ' . mysql_error());
mysql_query('SET NAMES utf8');

/** Import pliku schema **/
$filename = 'dump.sql';
$templine = '';
$lines = file($filename);
foreach ($lines as $line)
{
	if (substr($line, 0, 2) == '--' || $line == '')
	    continue;

	$templine .= $line;
	if (substr(trim($line), -1, 1) == ';')
	{
	    mysql_query($templine) or print('Error performing query \'<strong>' . $templine . '\': ' . mysql_error() . '<br /><br />');
	    $templine = '';
	}
}

/** Wygenerowanie danych admina **/
$adminMail = $input->env->HttpVhost->subdomena . '@dpoczta.pl';
$adminPass = substr(md5(sha1(date(DATE_ATOM).'dh123')), 0, 8);
$wp_hasher = new PasswordHash(8, true);
$hashedAdminPass = $wp_hasher->HashPassword( trim( $adminPass ) );

/** Aktualizacja adresu strony **/
$siteUrl = 'http://' . $input->env->HttpVhost->subdomena;
mysql_query('UPDATE wp_options SET option_value="'.$siteUrl.'" WHERE option_id IN (1,2)');
mysql_query('UPDATE wp_options SET option_value="'.$adminMail.'" WHERE option_id = 6');
/** Aktualizacja hasla admina **/
mysql_query('UPDATE wp_users SET user_email="'.$adminMail.'", user_pass="'.$hashedAdminPass.'" WHERE ID=1');

/** Aktualizacja pliku konfiguracyjnego **/
$configTpl = file_get_contents('wp-config.tpl');
$from = array('{DB_NAME}', '{DB_USER}', '{DB_PASS}', '{DB_HOST}');
$to = array($mysql_database, $mysql_username, $mysql_password, $mysql_host);
$configTpl = str_replace($from, $to, $configTpl);

file_put_contents('../wp-config.php', $configTpl);
unlink('../index.html');

$x = array(
	'AdminPanel' => array(
		'url' => $siteUrl . '/wp-admin/',
		'login' => 'admin',
		'haslo' => $adminPass,
	)
);

echo json_encode($x);
