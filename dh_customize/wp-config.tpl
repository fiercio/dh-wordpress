<?php
/**
 * Podstawowa konfiguracja WordPressa.
 *
 * Ten plik zawiera konfiguracje: ustawień MySQL-a, prefiksu tabel
 * w bazie danych, tajnych kluczy i ABSPATH. Więcej informacji
 * znajduje się na stronie
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Kodeksu. Ustawienia MySQL-a możesz zdobyć
 * od administratora Twojego serwera.
 *
 * Ten plik jest używany przez skrypt automatycznie tworzący plik
 * wp-config.php podczas instalacji. Nie musisz korzystać z tego
 * skryptu, możesz po prostu skopiować ten plik, nazwać go
 * "wp-config.php" i wprowadzić do niego odpowiednie wartości.
 *
 * @package WordPress
 */

// ** Ustawienia MySQL-a - możesz uzyskać je od administratora Twojego serwera ** //
/** Nazwa bazy danych, której używać ma WordPress */
define('DB_NAME', '{DB_NAME}');

/** Nazwa użytkownika bazy danych MySQL */
define('DB_USER', '{DB_USER}');

/** Hasło użytkownika bazy danych MySQL */
define('DB_PASSWORD', '{DB_PASS}');

/** Nazwa hosta serwera MySQL */
define('DB_HOST', '{DB_HOST}');

/** Kodowanie bazy danych używane do stworzenia tabel w bazie danych. */
define('DB_CHARSET', 'utf8');

/** Typ porównań w bazie danych. Nie zmieniaj tego ustawienia, jeśli masz jakieś wątpliwości. */
define('DB_COLLATE', '');

/**#@+
 * Unikatowe klucze uwierzytelniania i sole.
 *
 * Zmień każdy klucz tak, aby był inną, unikatową frazą!
 * Możesz wygenerować klucze przy pomocy {@link https://api.wordpress.org/secret-key/1.1/salt/ serwisu generującego tajne klucze witryny WordPress.org}
 * Klucze te mogą zostać zmienione w dowolnej chwili, aby uczynić nieważnymi wszelkie istniejące ciasteczka. Uczynienie tego zmusi wszystkich użytkowników do ponownego zalogowania się.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'X~Ii[.M$@/tOr_b1* pE]|&zH+svYE~i%y`Le:~BoB*ozKaxC:D(y fl)ij%X,e:');
define('SECURE_AUTH_KEY',  'Y?>B(;kF%RkPIGinDLj%_FlE#&)i)-6UxoQuaiG~jjir/CoZHetg?/jenP3pZof,');
define('LOGGED_IN_KEY',    ' b:E$C}N%p<+nwb$XWY>Rfp}a{dxbaTBP~b j0=oRb,g^t<$BY#m|!weA.rp`a6-');
define('NONCE_KEY',        '.L6~|J2DQfY%3=adjz-EW~*Y7{fc{<EHC;1E8-14zEY1`-7Y-ksP?5lzLCE43@ZD');
define('AUTH_SALT',        'XA2;F~;fFXH~[d3e m`+i{[BnK{Ee`2rS`~g*+$ihQGRK@zByr3|RBTZPv>)^l.g');
define('SECURE_AUTH_SALT', 'LEt{=!#TXq8{-:TA$myo:LhvGm^}5bDV8cn[tBnf?ki:na-hlerh@ujJ]yV(?Q_s');
define('LOGGED_IN_SALT',   '5RD4[_1|Nbr$@rv<UHQ[%hG5g>nT* $M)4+ofkbUAzfFoFZ/3js3q1jQzVmTovHs');
define('NONCE_SALT',       'U%yyU2O~*.%JJ^1neq=@RuFH_iPwmi+B7L7wr}cs#}VGosamg:s|]bWa=ptt+^E5');

/**#@-*/
/**
 * Prefiks tabel WordPressa w bazie danych.
 *
 * Możesz posiadać kilka instalacji WordPressa w jednej bazie danych,
 * jeżeli nadasz każdej z nich unikalny prefiks.
 * Tylko cyfry, litery i znaki podkreślenia, proszę!
 */
$table_prefix  = 'wp_';

/**
 * Dla programistów: tryb debugowania WordPressa.
 *
 * Zmień wartość tej stałej na true, aby włączyć wyświetlanie ostrzeżeń
 * podczas modyfikowania kodu WordPressa.
 * Wielce zalecane jest, aby twórcy wtyczek oraz motywów używali
 * WP_DEBUG w miejscach pracy nad nimi.
 */
define('WP_DEBUG', false);

/* To wszystko, zakończ edycję w tym miejscu! Miłego blogowania! */

/** Absolutna ścieżka do katalogu WordPressa. */
if ( !defined('ABSPATH') )
        define('ABSPATH', dirname(__FILE__) . '/');

/** Ustawia zmienne WordPressa i dołączane pliki. */
require_once(ABSPATH . 'wp-settings.php');
